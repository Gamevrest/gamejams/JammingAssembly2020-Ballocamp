﻿// using GameManagerSystem;

using System;
using GameManagerSystem;
using GamevrestUtils;
using Mirror;
using UnityEngine;

namespace UI
{
    public class SettingsManager : MonoBehaviour
    {
        [Header("Config")] public KeyCode shortcut;
        public GameObject settingsPanel;
        public bool enableSettings = true;
        [ReadOnly] public NetworkGameManager manager;
        public bool stopTime;

        [Header("references")] [ReadOnly] public bool isOpen;
        [ReadOnly] public SceneManager sceneManager;
        public SceneReference mainMenu;

        private void Start()
        {
            sceneManager = PersistentObject.GetSceneManager();
            CloseSettings();
        }

        private void Update()
        {
            //todo remplacer par un vrai truc avec new input system
            if (Input.GetKeyDown(shortcut))
            {
                if (isOpen) CloseSettings();
                else OpenSettings();
            }
        }

        public void OpenSettings()
        {
            if (!enableSettings) return;
            isOpen = true;
            if (stopTime) Time.timeScale = 0;
            settingsPanel.SetActive(true);
        }

        public void CloseSettings()
        {
            isOpen = false;
            if (stopTime) Time.timeScale = 1;
            settingsPanel.SetActive(false);
        }

        public void MainMenu()
        {
            if (stopTime) Time.timeScale = 1;
            if (!manager) manager = FindObjectOfType<NetworkGameManager>();
            switch (manager.mode)
            {
                case NetworkManagerMode.Host:
                    manager.StopHost();
                    break;
                case NetworkManagerMode.ClientOnly:
                    manager.StopClient();
                    break;
                case NetworkManagerMode.Offline:
                    sceneManager.LoadScene(mainMenu);
                    break;
                case NetworkManagerMode.ServerOnly:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Save()
        {
            // sceneManager.Save();
        }
    }
}