﻿using GameManagerSystem;
using UnityEngine;

namespace UI
{
    public class MainMenuManager : MonoBehaviour
    {
        [Header("General")] public GameObject creditsCanvas;
        [Header("Scenes")] public SceneReference GameScene;

        public void ShowCredits() => creditsCanvas.SetActive(true);
        public void HideCredits() => creditsCanvas.SetActive(false);

        public void Play()
        {
             PersistentObject.GetSceneManager().LoadScene(GameScene);
        }
    }
}