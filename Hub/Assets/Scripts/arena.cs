﻿using GamevrestUtils;
using Mirror;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEngine;

public class arena : MonoBehaviour
{
    [Header("Grid settings")] public float radius = 0.5f;
    public bool useAsInnerCircleRadius = true;
    private float _offsetX, _offsetY;
    public GameObject prefab;

    [Header("Players settings")] [Range(2, 12)] [ReadOnly]
    public int nbPlayer = 2;

    public GameObject startPrefab;
    public GameObject containerPrefab;
    [Range(0, 1)] public float transparency;

    public int spawnCircleSize = 1;
    [ReadOnly] public Vector2Int[] PlayersPos;
    public Transform[] playerContainers;
    public int[] PlayerPoints;

    [Header("arena settings")] public int arenaSize;
    [Range(2, 13)] public int startAreaSize = 4;
    public Color arenaColor;
    public GameObject arenaPrefab;
    public Transform arenaContainer;

    [Header("other settings")] public Transform testContainer;
    [ReadOnly] public NetworkGameManager manager;

    [ReadOnly]public Color[] playerColor;
    private void Start()
    {
        manager = FindObjectOfType<NetworkGameManager>();
        if (manager) nbPlayer = manager.PlayerCount;
        playerColor = new Color[nbPlayer];
        Clean();
        Calculate();
        CreateGame();
    }

    public void PlayerSetZone(int player, int value, Color c)
    {
        PlayerPoints[player] = value;
        PlayerUpdateZone(player, c);
    }
    public void PlayerUpdateZone(int player, Color c, int value = 0)
    {
        PlayerPoints[player] += value;
        playerColor[player] = c;
        DrawHexagon(PlayersPos[player], PlayerPoints[player], c, playerContainers[player]);
    }

    public void Calculate()
    {
        var unitLength = (useAsInnerCircleRadius) ? (radius / (Mathf.Sqrt(3) / 2)) : radius;
        _offsetX = unitLength * Mathf.Sqrt(3);
        _offsetY = unitLength * 1.5f;
    }

    private Vector2 HexOffset(int x, int y)
    {
        var position = Vector2.zero;
        if (y % 2 == 0)
        {
            position.x = x * _offsetX;
            position.y = y * _offsetY;
        }
        else
        {
            position.x = (x + 0.5f) * _offsetX;
            position.y = y * _offsetY;
        }

        return position;
    }

    private void CreateGame()
    {
        var spacing = 2 * Mathf.PI / nbPlayer;
        PlayersPos = new Vector2Int[nbPlayer];
        playerContainers = new Transform [nbPlayer];
        PlayerPoints = new int[nbPlayer];
        for (var i = 0; i < nbPlayer; i++)
        {
            var obj = Instantiate(containerPrefab, Vector3.one, Quaternion.identity, transform);
            playerContainers[i] = obj.transform;
            obj.name = $"Area player {i}";
            var ang = spacing * i;
            PlayersPos[i] = new Vector2Int((int) (Mathf.Cos(ang) * spawnCircleSize),
                (int) (Mathf.Sin(ang) * spawnCircleSize));
            var hexa = HexOffset(PlayersPos[i].x, PlayersPos[i].y);
            print($"nb:{i} angle:{ang * Mathf.Rad2Deg} pos:{PlayersPos[i]} hexa:{hexa}");
            PlayerSetZone(i, startAreaSize, Color.green);
            Instantiate(startPrefab, GamevrestTools.V2toV3(hexa), Quaternion.identity,
                playerContainers[i]);
        }
    }

    private bool IsInArena(Vector2 pos)
    {
        return pos.magnitude < spawnCircleSize;
    }

    private void InstantiateTileImmediate(Vector2Int curPos, Color color, Transform container, bool isArena = false)
    {
        var tr = HexOffset(curPos.x, curPos.y);
        if (!isArena && !IsInArena(tr)) return;
        var obj = isArena ? arenaPrefab : prefab;
        var tmp = Instantiate(obj, new Vector3(tr.x, tr.y, 0), Quaternion.identity, container);
        tmp.name = $"{curPos.x}:{curPos.y}->{tr.x}:{tr.y}";
        var spriteRenderer = tmp.GetComponent<SpriteRenderer>();
        if (!isArena)
            color.a = transparency;
        spriteRenderer.color = color;
        spriteRenderer.sortingOrder = isArena ? 0 : 1;
        var position = tmp.transform.position;
        tmp.transform.position = position;
    }

    public void DrawSquare(Vector2Int pos, int size, Color color, Transform container, bool isArena = false)
    {
        GamevrestTools.CleanChildren(container);
        var start = size / 2;
        for (var x = -start; x < start; x++)
        for (var y = -start; y < start; y++)
            InstantiateTileImmediate(pos + new Vector2Int(x, y), color, container, isArena);
    }

    public void DrawDiamond(Vector2Int pos, int size, Color color, Transform container, bool isArena = false)
    {
        GamevrestTools.CleanChildren(container);
        for (var y = -size; y <= size; y++)
        {
            var abs = Mathf.Abs(y - pos.y);
            var start = abs / 2;
            for (var x = start; x < start + size - abs; x++)
                InstantiateTileImmediate(pos + new Vector2Int(x, y), color, container, isArena);
        }
    }

    public void DrawHexagon(Vector2Int pos, int size, Color color, Transform container, bool isArena = false)
    {
        GamevrestTools.CleanChildren(container);
        pos.x -= size;
        var height = size - 1;
        var width = size * 2 - 1;
        for (var y = -height; y <= height; y++)
        {
            var abs = Mathf.Abs(y);
            var start = abs / 2;
            for (var x = start + 1; x <= start + width - abs; x++)
                InstantiateTileImmediate(pos + new Vector2Int(x, y), color, container, isArena);
        }
    }

    public void Clean()
    {
        GamevrestTools.CleanChildren(testContainer);
        foreach (var child in playerContainers)
            GamevrestTools.CleanChildren(child);
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(arena))]
public class arenaEditor : Editor
{
    private Vector2Int _position;
    private int _size;
    private int _player;
    private Color _color = Color.white;
    private EditorCoroutine curCoroutine;

    public override void OnInspectorGUI()
    {
        var targ = target as arena;
        if (targ == null) return;
        base.OnInspectorGUI();

        CustomEditorUtils.DrawUiLine(Color.black, 2);
        CustomEditorUtils.DrawHeader("Editeur");
        _position = EditorGUILayout.Vector2IntField("position", _position);
        _size = EditorGUILayout.IntField("size", _size);
        _color = EditorGUILayout.ColorField("Color", _color);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("clean containers"))
            targ.Clean();
        if (GUILayout.Button("create arena"))
        {
            targ.Calculate();
            targ.DrawHexagon(Vector2Int.zero, targ.arenaSize, targ.arenaColor, targ.arenaContainer, true);
        }

        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("square"))
        {
            targ.Calculate();
            targ.DrawSquare(_position, _size, _color, targ.testContainer);
        }

        if (GUILayout.Button("diamond"))
        {
            targ.Calculate();
            targ.DrawDiamond(_position, _size, _color, targ.testContainer);
        }

        if (GUILayout.Button("hexagon"))
        {
            targ.Calculate();
            targ.DrawHexagon(_position, _size, _color, targ.testContainer);
        }

        GUILayout.EndHorizontal();
        CustomEditorUtils.DrawHeader("players");
        _player = EditorGUILayout.IntField("player", _player);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("augment"))
            
            targ.PlayerUpdateZone(_player, targ.playerColor[_player] , 1);
        if (GUILayout.Button("decrease"))
            targ.PlayerUpdateZone(_player, targ.playerColor[_player],-1);
        GUILayout.EndHorizontal();
    }
}
#endif