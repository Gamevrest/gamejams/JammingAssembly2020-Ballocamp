﻿using System;
using System.Collections.Generic;
using GamevrestUtils;
using UnityEngine;
using UnityEngine.Events;

namespace EventsSystem
{
    [Serializable]
    public class EventsManager
    {
        [ReadOnly] public List<GameEvent> events;

        public void AddEvent(GameEvent @event)
        {
            if (events.Contains(@event))
            {
                //Debug.Log($"Event [{@event}] wasn't added because it already exists");
                return;
            }

            events.Add(@event);
            Debug.Log($"Event [{@event}] was added to list");
        }

        public GameEventListener RegisterEvent(string name, UnityAction<Payload> callback)
        {
            var evt = GetEvent(name);
            return evt == null ? null : evt.Register(new GameEventListener(evt, new PayloadUnityEvent(callback)));
        }

        public GameEvent GetEvent(string name)
        {
            foreach (var @event in events)
                if (@event.name == name)
                    return @event;

            Debug.LogWarning($"Couldn't find event [{name}]");
            return null;
        }

        public void Raise(string name, Payload p) => GetEvent(name).Raise(p);

        //Shortcuts for easy use
        public void Raise(string name) => GetEvent(name).Raise();
        public void Raise(string name, string s) => GetEvent(name).Raise(s);
        public void Raise(string name, int i) => GetEvent(name).Raise(i);
        public void Raise(string name, float f) => GetEvent(name).Raise(f);
    }
}