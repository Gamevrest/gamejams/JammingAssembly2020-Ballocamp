﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace EventsSystem
{
    [Serializable]
    public class PayloadUnityEvent : UnityEvent<Payload>
    {
        public PayloadUnityEvent()
        {
        }

        public PayloadUnityEvent(UnityAction<Payload> call)
        {
            AddListener(call);
        }
    }

    [Serializable]
    public class GameEventListener
    {
        public GameEvent @event;
        public PayloadUnityEvent response;

        public GameEventListener(GameEvent @event, PayloadUnityEvent response)
        {
            this.@event = @event;
            this.response = response;
        }

        public void UnregisterSelf() => @event.Unregister(this);
        public void OnEventRaised(GameEvent evt, Payload payload) => response.Invoke(payload);
    }

    public class GameEventListenerBehaviour : MonoBehaviour
    {
        public List<GameEventListener> eventsToListen = new List<GameEventListener>();
        private void OnEnable() => eventsToListen.ForEach(c => c.@event.Register(c));
        private void OnDisable() => eventsToListen.ForEach(c => c.@event.Unregister(c));
    }
}