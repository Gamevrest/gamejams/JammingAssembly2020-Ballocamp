﻿using System.Collections.Generic;
using EventsSystem.Payloads;
using GamevrestUtils;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace EventsSystem
{
    [CreateAssetMenu(menuName = "Scriptables/Game Event")]
    public class GameEvent : ScriptableObject
    {
        public static bool DebugEvents;
        [SerializeField] private List<GameEventListener> listeners = new List<GameEventListener>();
        
        public GameEventListener Register(GameEventListener l)
        {
            listeners.Add(l);
            return l;
        }

        public void Unregister(GameEventListener l) => listeners.Remove(l);

        // private void OnValidate() => GameManager.Instance.eventsManager.AddEvent(this);
        
        private void PrintDebug(Payload payload)
        {
            switch (payload)
            {
                case IntPayload intPayload:
                    Debug.Log($"RAISE {name} with {typeof(Payload)} : [{intPayload.Data}]");
                    break;
                case StringPayload stringPayload:
                    Debug.Log($"RAISE {name} with {typeof(Payload)} : [{stringPayload.Data}]");
                    break;
                default:
                    Debug.Log($"RAISE {name} with {typeof(Payload)}");
                    break;
            }
        }

        public void Raise(Payload payload)
        {
            if (DebugEvents) PrintDebug(payload);
            foreach (var listener in listeners)
                listener.OnEventRaised(this, payload);
        }

        //Helpers for standard payloads
        public void Raise() => Raise(new Payload());
        public void Raise(string s) => Raise(new StringPayload(s));
        public void Raise(int i) => Raise(new IntPayload(i));
        public void Raise(float f) => Raise(new FloatPayload(f));
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(GameEvent))]
    public class GameEventEditor : Editor
    {
        private string _st;
        private int _it;
        private float _ft;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var gameEvent = target as GameEvent;
            if (gameEvent == null)
                return;
            CustomEditorUtils.DrawHeader("Options");
            GameEvent.DebugEvents = EditorGUILayout.Toggle("Should debug events", GameEvent.DebugEvents);
            CustomEditorUtils.DrawHeader("Testers");
            if (GUILayout.Button("Raise empty") && gameEvent != null)
                gameEvent.Raise();
            using (new GUILayout.HorizontalScope())
            {
                _st = EditorGUILayout.TextField(_st);
                if (GUILayout.Button("Raise string"))
                    gameEvent.Raise(_st);
            }

            using (new GUILayout.HorizontalScope())
            {
                _it = EditorGUILayout.IntField(_it);
                if (GUILayout.Button("Raise int"))
                    gameEvent.Raise(_it);
            }

            using (new GUILayout.HorizontalScope())
            {
                _ft = EditorGUILayout.FloatField(_ft);
                if (GUILayout.Button("Raise float"))
                    gameEvent.Raise(_ft);
            }
        }
    }
#endif
}