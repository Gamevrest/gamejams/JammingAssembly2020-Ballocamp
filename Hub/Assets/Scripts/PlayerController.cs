﻿using GamevrestUtils;
using Mirror;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerController : NetworkBehaviour
{
    private Material _cachedMaterial;
    [SyncVar(hook = nameof(ChangeColor))] public Color _playerColor = Color.red;

    private bool hasBall = false;
    private GameObject ball = null;

    [ReadOnly] public Rigidbody2D _rigid;
    public float acceleration = 5f;
    public float stopSpeed = 0.5f;
    public float maxSpeed = 10f;

    private Vector2 mov;
    /*
    public override void OnStartServer()
    {
        base.OnStartServer();
        _playerColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        OnStart();
    }

    public void OnStart()
    {
        _rigid = GetComponent<Rigidbody2D>();
        name = !isLocalPlayer ? "network player" : "LOCAL PLAYER";
    }
    */
    public override void OnStartServer()
    {
        base.OnStartServer();
        _playerColor = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        OnStart();
    }
    
    public void OnStart()
    {
        _rigid = GetComponent<Rigidbody2D>();
        name = !isLocalPlayer ? "network player" : "LOCAL PLAYER";
    }

    public void ChangeColor(Color oldColor, Color newColor)
    {
        if (_cachedMaterial == null)
            _cachedMaterial = GetComponent<SpriteRenderer>().material;
        _cachedMaterial.color = newColor;
    }

    private void FixedUpdate()
    {
        if (!isLocalPlayer) return;
        if (hasBall && ball != null)
            ball.transform.position = transform.position;
        mov = Vector2.zero;

        mov.x += Input.GetAxisRaw("Horizontal") < 0 ? -1 : 0;
        mov.x += Input.GetAxisRaw("Horizontal") > 0 ? 1 : 0;
        mov.y += Input.GetAxisRaw("Vertical") < 0 ? -1 : 0;
        mov.y += Input.GetAxisRaw("Vertical") > 0 ? 1 : 0;

        if (mov == Vector2.zero && (Mathf.Abs(_rigid.velocity.x) > 0.00f
                                    || Mathf.Abs(_rigid.velocity.y) > 0.00f))
            _rigid.velocity *= stopSpeed;

        _rigid.AddForce(mov.normalized * acceleration);
        if (_rigid.velocity.magnitude > maxSpeed)
            _rigid.velocity = _rigid.velocity.normalized * maxSpeed;
        if (Input.GetKeyDown(0) || Input.GetKeyDown(KeyCode.Space))
            ThrowBall();
    }

    private void OnDestroy()
    {
        Destroy(_cachedMaterial);
    }

    private void ThrowBall()
    {
        if (!hasBall)
            return;
        hasBall = false;
        ball.GetComponent<Ball>().GetThrown(10, mov);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Balle")
        {
            hasBall = true;
            ball = other.gameObject;
        }
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(PlayerController))]
public class PlayerControllerEditor : Editor
{
    private Vector2Int position;
    private int size;

    public override void OnInspectorGUI()
    {
        var targ = target as PlayerController;
        base.OnInspectorGUI();
        // if (GUILayout.Button("Maj Color"))
        // {
        //     targ.ChangeColor(Color.black, targ._playerColor);
        // }
    }
}
#endif