﻿using UnityEngine;

public class Ball : MonoBehaviour//NetworkBehaviour
{
    public PlayerController lastPlayer;

    public float baseSpeed;
    private Rigidbody2D rigid;
    private void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
        //rigid.AddForce(transform.right * 100 * baseSpeed);
    }

    void Update()
    {
        if (Mathf.Abs(rigid.velocity.x) > 1f || Mathf.Abs(rigid.velocity.y) > 1f)
            rigid.velocity *= 0.995f;
    }

    void GetThrown()
    {
        rigid.AddForce(transform.up * 100 * baseSpeed);
    }
    
    public void GetThrown(float speed, Vector2 mov)
    {
        rigid.AddForce(mov * 100 * speed);
    }
}
