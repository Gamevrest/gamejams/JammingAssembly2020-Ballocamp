﻿using EventsSystem;
using Mirror;
using Unity.Collections;
using UnityEngine;

public class NetworkGameManager : NetworkRoomManager
{

    
    [ReadOnly]public int PlayerCount;
    private bool forceRedraw = true;
    public class PlayersUpdateMessage : NetworkMessage
    {
        public int[] PlayerPoints;
        public Color[] color;
        public bool force;
    }
    
    public class PlayerCountMessage : NetworkMessage
    {
        public int count;
    }
    public enum GameState
    {
        MainMenu,
        Lobby,
        Playing,
        Ended
    }
    

    public string currentPlayerName;
    
    public GameEvent stateChange;
    public GameEvent disconnectEvent;

    public GameState state;
    private arena _arena;
    
    public override void Awake()
    {
        base.Awake();
        UpdateGameState();
        NetworkClient.RegisterHandler<PlayersUpdateMessage>(OnPlayersUpdate);
        NetworkClient.RegisterHandler<PlayerCountMessage>(OnPlayerCountMessage);
    }

    private void OnPlayerCountMessage(PlayerCountMessage obj)
    {
        PlayerCount = obj.count;
    }

    private void OnPlayersUpdate(PlayersUpdateMessage update)
    {
        if (!_arena) _arena = FindObjectOfType<arena>();
        var playerPoints = update.PlayerPoints;
        var color = update.color;
        for (var i = 0; i < playerPoints.Length; i++)
        {
            if (_arena.PlayerPoints[i] != playerPoints[i] || update.force)
            {
                _arena.PlayerSetZone(i, playerPoints[i], color[i]);
                _arena.PlayerPoints[i] = playerPoints[i];
            }
        }
    }
    
    [Server]
    private void SendPlayersPointUpdate()
    {
        if (!_arena) _arena = FindObjectOfType<arena>();
        var players = FindObjectsOfType<PlayerController>();
        var colors = new Color[players.Length];
        for (var i = 0; i < players.Length; i++)
        {
            colors[i] = players[i]._playerColor;
        }
       
        NetworkServer.SendToAll(new PlayersUpdateMessage()
        {
            PlayerPoints = _arena.PlayerPoints,
            color = colors,
            force = forceRedraw,
        });
        forceRedraw = false;
    }

    private void UpdateGameState()
    {
        if (IsSceneActive(RoomScene))
        {
            forceRedraw = true;
            state = GameState.Lobby;
            if (mode == NetworkManagerMode.Host)
                CancelInvoke();

        } else if (IsSceneActive(GameplayScene))
        {
            state = GameState.Playing;
        }
        else
        {                
            if (mode == NetworkManagerMode.Host)
                CancelInvoke();
            state = GameState.MainMenu;
        }
        Debug.LogWarning($"UpdateGameState => {state}");
        stateChange.Raise();
    }
    
    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        UpdateGameState();
        
        base.OnClientSceneChanged(conn);
    }

    public override void OnServerSceneChanged(string sceneName)
    {
        UpdateGameState();
        if (mode == NetworkManagerMode.Host && IsSceneActive(GameplayScene))
            InvokeRepeating(nameof(SendPlayersPointUpdate), 1f, 0.10f);
        base.OnServerSceneChanged(sceneName);
    }

    public override void OnServerReady(NetworkConnection conn)
    {
        base.OnServerReady(conn);
        NetworkServer.SendToAll(new PlayerCountMessage()
        {
            count = maxConnections
        });
    }

    public void SetPlayerCount(int value)
    {
        maxConnections = value;
        minPlayers = value;
        PlayerCount = value;
    }

    public override void OnRoomClientDisconnect (NetworkConnection conn)
    {
        base.OnRoomClientDisconnect(conn);
        disconnectEvent.Raise();
    }
}