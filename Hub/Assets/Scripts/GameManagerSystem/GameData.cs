﻿using GamevrestUtils;
using SaveSystem;
using UnityEngine;

namespace GameManagerSystem
{
    public class GameData : MonoBehaviour
    {
        [Header("Game Data")] [ReadOnly] public SaveData savedData;
        [Header("Firebase")] public string databaseUrl;
        public string usersId = "users";

        private void Awake()
        {
#if UNITY_EDITOR
            Load();
#endif
        }

        public void New()
        {
            savedData = new SaveData { };
        }

        public void Save()
        {
            SaveManager.Save(savedData);
        }

        public void Load()
        {
            savedData = SaveManager.Load();
            if (string.IsNullOrEmpty(savedData.userId))
                savedData.userId = "EDITOR";
        }
    }
}