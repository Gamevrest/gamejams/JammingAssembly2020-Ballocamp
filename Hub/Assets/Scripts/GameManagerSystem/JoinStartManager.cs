﻿using System;
using GameManagerSystem;
using Mirror;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class JoinStartManager : MonoBehaviour
{
    public NetworkGameManager manager;

    public TextMeshProUGUI joinText;
    public InputField address;
    public Slider playerCount;
    public TextMeshProUGUI hostText;

    private void Awake()
    {
        manager = FindObjectOfType<NetworkGameManager>();
        UpdateJoinText();
        address.text = manager.networkAddress;
        playerCount.value = manager.maxConnections;
        hostText.text = $"Host Game\nwith {playerCount.value} players";
    }

    public void UpdateJoinText()
    {
        Debug.Log("UPDATE TEXT");
        if (NetworkClient.active)
        {
            joinText.text = "Cancel Join";
        }
        else
        {
            joinText.text = "Join Game";
        }
    }

    public void JoinStop()
    {
        if (!NetworkClient.active)
        {
            manager.StartClient();
        }
        else
        {
            manager.StopClient();
        }

        UpdateJoinText();
    }

    public void StartHost()
    {
        if (!NetworkClient.active)
        {
            manager.StartHost();
        }
    }
    
    public void SetNetworkAddress()
    {
        manager.networkAddress = address.text;
    }
    
    public void SetPlayerCount()
    {
        manager.SetPlayerCount((int) playerCount.value);
        hostText.text = $"Host Game\nwith {playerCount.value} players";
    }
}