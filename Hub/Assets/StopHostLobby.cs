﻿using Mirror;
using TMPro;
using UnityEngine;

public class StopHostLobby : MonoBehaviour
{
    private NetworkGameManager _manager;

    private void Awake()
    {
        _manager = FindObjectOfType<NetworkGameManager>();
        var text = GetComponentInChildren<TextMeshProUGUI>();
        switch (_manager.mode)
        {
            case NetworkManagerMode.Host:
                text.text = "Stop Host";
                break;
            case NetworkManagerMode.ClientOnly:
                text.text = "Stop Client";
                break;
        }
    }

    public void StopHost()
    {
        switch (_manager.mode)
        {
            case NetworkManagerMode.Host:
                _manager.StopHost();
                break;
            case NetworkManagerMode.ClientOnly:
                _manager.StopClient();
                break;
        }
    }
}
