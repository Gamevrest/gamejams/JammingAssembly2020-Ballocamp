$src = ".\Hub"
$dest = ".\TempUnity"
$editor = "D:\Program Files\UnityEditor\2020.1.13f1\Editor\Unity.exe"

cmd /c rmdir -rf $dest

mkdir $dest

cmd /c mklink /J "$dest\Assets" "$src\Assets"  
cmd /c mklink /J "$dest\ProjectSettings" "$src\ProjectSettings"
cmd /c mklink /J "$dest\Packages" "$src\Packages"

& $editor -projectPath $dest
pause